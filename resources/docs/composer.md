# Composer

Composer related stuff.

## Up to date?

Are the vendor packages up to date to the composer.json?

```bash

#!/usr/bin/env sh
# Shell: (sh composerUpToDate.sh)

# First simple syntax checks
binPHP=$(which php)
errorCount=0
rm -f var/tmp/temp-syntax-test-files.txt
find $(pwd) -iname "*.php" -type f -print | grep -Eiv "(var|vendor)/" | sort > var/tmp/temp-syntax-test-files.txt
for line in `cat var/tmp/temp-syntax-test-files.txt`
do
  count=$($binPHP -l $line | grep -iv "no syntax errors" | wc -l)
  errorCount=$(( $count + $errorCount ))
done

rm -f var/tmp/temp-syntax-test-files.txt
if [ $errorCount -gt 0 ]
then
  exit 1;
fi
exit 0;

composer update --dry-run --no-ansi --no-scripts | grep -Ei "\d+\s+(installs|removal)"

```
