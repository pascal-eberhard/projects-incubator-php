######################################################
PHP phar apps:
  _meta:
    ideas:
      - ffmpegWrapper
      - pandocWrapper
      - teseractWrapper imageToText software
      - YAML CSV, field into in _meta enty, data in array, without meta data
      - Minify data with meta data to bitmap
      bitMapMinify:
        - Minify data with meta data to bitmap
        - Example, URL string, bitmap for URL schemes
      imageMagicWrapper:
        - use portable imageMagic
        - Add PHP controller with error handling
    questions:
      - Set PHP callback via config?
      - Some tools better as java .jar for better performance?
      - Linux command line piping with PHP, with additional .sh - for the php file?
    todos:
      - Logging instead of error output
      - https://stackoverflow.com/questions/6107339/parallel-processing-in-php-how-do-you-do-it
      - https://www.php.net/manual/de/ref.pcntl.php
      - https://github.com/krakjoe/pthreads
  behatTool:
    - "See entry \"fileFinder\""
    - Download Javascript based web pages via behat
    - behat + selenium server standAlone + chrome + chromeDriver
  databaseImport:
    - "See entry \"fileFinder\""
    - Convert given input data into insert SQL queries
    - Further processing via callbacks? If possible via config file
  dataConverter:
    - "See entry \"fileFinder\""
    - Convert one data format to another
    - JSON to YAML etc.
    - Usefull to impoort different data, e.g. different config file formats
  fileDownloader:
    - "See entry \"fileFinder\""
    - Download files from list of URLs
  fileFinder:
    - Based at symfony finder
    - Or at linux find
    - Configure with config file (default YAML)
    - Shared settings for find and finder
    - Exclusive settings for find or finder
    - Output data, to commandline or file, CSV, TXT, JSON, PHP, YAML, XML
  fileInfoFinder:
    - Based at fileFinder service
    - Configure what infos to get
    - Basics as path parts
    - Size, readAble, writeAble, file type, mime type
    - Specials, imageMagic infos for images
    - Specials, ffmpeg infos for videos
    - Perhaps also as Java .jar, for better performance + parallell excectuin?
  hashTool:
    - "See entry \"fileFinder\""
    - Hash differen data
    - Strings
    - Files
    - Whole directories, alternativelly .tar files
    - For directories we must ensure normalized data, sort all pathes by name
    - Get directory content and/or structure hash
  minifyTool:
    - "See entry \"fileFinder\""
    - Minify content and files, e.g. no pretty print JSON
  syntaxChecker:
    - "See entry \"fileFinder\""
    - Check syntax with easy methods or with extern tools, linter, nodejs etc.
  templateBuilder:
    - "See entry \"fileFinder\""
    - Default engine Symfony twig
    - Create content and files from templaates
    - Configure with config file
  urlInfos:
    - "See entry \"fileFinder\""
    - URL string lists from input, command line or file
    - Normalize and validate the URLs
    - Convert the input into info data sets
    - URL parts
    - Is file (by URL string format, depends at server if URL is called)
    - Further processing via callbacks? If possible via config file

Create jar files + YAML config for Java:
- https://github.com/Carleslc/Simple-YAML
- https://github.com/jsixface/YamlConfig
- https://www.jetbrains.com/help/idea/compiling-applications.html#compile_before_launch
- https://stackoverflow.com/questions/59399468/bitbucket-pipelines-to-build-java-app-docker-image-and-push-it-to-aws-ecr
- https://support.atlassian.com/bitbucket-cloud/docs/java-with-bitbucket-pipelines/
- https://community.atlassian.com/t5/Bitbucket-questions/How-do-I-create-a-docker-image-for-Bitbucket-Pipelines/qaq-p/334724?tempId=eyJvaWRjX2NvbnNlbnRfbGFuZ3VhZ2VfdmVyc2lvbiI6IjIuMCIsIm9pZGNfY29uc2VudF9ncmFudGVkX2F0IjoxNTkyOTU2NjIxNDUyfQ%3D%3D

TODO better bitbucket imagge https://github.com/pyguerder/bitbucket-pipelines-php71

bitbucket pages
Image compressiong;https://github.com/maksatweb/compressor.io-php
SVG slide show;https://github.com/codeclou/hirngespinst/
SVG slide show/shell;https://github.com/mhelsley/SVGSlides
Optimize SVG;https://github.com/svg/svgo
CHECK https://github.com/jakearchibald/svgomg
SVG inline via base64;<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNi4wLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB3aWR0aD0iMTI2cHgiIGhlaWdodD0iMTI2cHgiIHZpZXdCb3g9IjAgMCAxMjYgMTI2IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMjYgMTI2IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxyZWN0IHg9IjEuMDk1IiB5PSI5OC4yMjQiIHdpZHRoPSIxMjMuODEiIGhlaWdodD0iMTkuMjc1Ii8+DQoJPHJlY3QgeD0iMS4wOTUiIHk9Ijg1Ljc0IiB3aWR0aD0iMTIzLjgxIiBoZWlnaHQ9IjUuMjA1Ii8+DQoJPHBhdGggZD0iTTE4LjQwNCw5NS43MjFjMC43NjcsMCwxLjM4OS0wLjYyMywxLjM4OS0xLjM5cy0wLjYyMi0xLjM4OC0xLjM4OS0xLjM4OEgzLjQ4MWMtMC43NjcsMC0xLjM4OCwwLjYyMS0xLjM4OCwxLjM4OA0KCQlzMC42MjIsMS4zOSwxLjM4OCwxLjM5SDE4LjQwNHoiLz4NCgk8cGF0aCBkPSJNNDQuNDMzLDk1LjcyMWMwLjc2NywwLDEuMzg4LTAuNjIzLDEuMzg4LTEuMzlzLTAuNjIyLTEuMzg4LTEuMzg4LTEuMzg4SDI5LjUxYy0wLjc2NywwLTEuMzg5LDAuNjIxLTEuMzg5LDEuMzg4DQoJCXMwLjYyMiwxLjM5LDEuMzg5LDEuMzlINDQuNDMzeiIvPg0KCTxwYXRoIGQ9Ik03MC40NjEsOTUuNzIxYzAuNzY3LDAsMS4zODgtMC42MjMsMS4zODgtMS4zOXMtMC42MjItMS4zODgtMS4zODgtMS4zODhINTUuNTM5Yy0wLjc2NywwLTEuMzg4LDAuNjIxLTEuMzg4LDEuMzg4DQoJCXMwLjYyMiwxLjM5LDEuMzg4LDEuMzlINzAuNDYxeiIvPg0KCTxwYXRoIGQ9Ik05Ni40OSw5NS43MjFjMC43NjcsMCwxLjM4OS0wLjYyMywxLjM4OS0xLjM5cy0wLjYyMi0xLjM4OC0xLjM4OS0xLjM4OEg4MS41NjdjLTAuNzY3LDAtMS4zODgsMC42MjEtMS4zODgsMS4zODgNCgkJczAuNjIyLDEuMzksMS4zODgsMS4zOUg5Ni40OXoiLz4NCgk8cGF0aCBkPSJNMTIyLjUxOSw5NS43MjFjMC43NjcsMCwxLjM4OS0wLjYyMywxLjM4OS0xLjM5cy0wLjYyMi0xLjM4OC0xLjM4OS0xLjM4OGgtMTQuOTIzYy0wLjc2NywwLTEuMzg4LDAuNjIxLTEuMzg4LDEuMzg4DQoJCXMwLjYyMiwxLjM5LDEuMzg4LDEuMzlIMTIyLjUxOXoiLz4NCgk8cGF0aCBkPSJNNy40MSw4MC45aDUzLjQ0MmMwLjg2MywwLDEuNTYyLTAuNjk5LDEuNTYyLTEuNTYyVjM5LjU0M2MwLTAuODYyLTAuNjk5LTEuNTYzLTEuNTYyLTEuNTYzSDQ1LjMxNHYtNi41MzkNCgkJYzAtMC44NjEtMC42OTgtMS41NjItMS41NjEtMS41NjJIMjMuNDI4Yy0wLjg2MywwLTEuNTYyLDAuNy0xLjU2MiwxLjU2MnY2LjU0SDcuNDFjLTAuODYyLDAtMS41NjIsMC43LTEuNTYyLDEuNTYzdjM5Ljc5NQ0KCQlDNS44NDgsODAuMjAxLDYuNTQ3LDgwLjksNy40MSw4MC45eiBNMzQuNDkyLDU3Ljg3NGgtMS43OTZ2LTYuNzY4aDEuNzk2VjU3Ljg3NHogTTI2LjU2MywzNC41NzRoMTQuMDU1djMuNDA2SDI2LjU2M1YzNC41NzR6DQoJCSBNMTAuNTQ0LDQyLjY3OGg0Ny4xNzN2MTEuOThIMzYuOTQydi00LjAwNmMwLTAuODYzLTAuNjk5LTEuNTYzLTEuNTYyLTEuNTYzaC0zLjU4MmMtMC44NjMsMC0xLjU2MiwwLjY5OS0xLjU2MiwxLjU2M3Y0LjAwNg0KCQlIMTAuNTQ0VjQyLjY3OHoiLz4NCgk8cGF0aCBkPSJNNjguNzM0LDgwLjloNDkuOTU4YzAuODA3LDAsMS40Ni0wLjY1MywxLjQ2LTEuNDZWMTcuNTM0YzAtMC44MDYtMC42NTMtMS40NTktMS40Ni0xLjQ1OWgtMTQuNTI0VjkuOTYxDQoJCWMwLTAuODA3LTAuNjUzLTEuNDYtMS40Ni0xLjQ2aC0xOWMtMC44MDcsMC0xLjQ2LDAuNjUzLTEuNDYsMS40NnY2LjExNUg2OC43MzRjLTAuODA3LDAtMS40NiwwLjY1My0xLjQ2LDEuNDU5Vjc5LjQ0DQoJCUM2Ny4yNzQsODAuMjQ3LDY3LjkyNyw4MC45LDY4LjczNCw4MC45eiBNODYuNjM4LDEyLjg5aDEzLjEzOXYzLjE4Nkg4Ni42MzhWMTIuODl6Ii8+DQo8L2c+DQo8L3N2Zz4NCg=="/>
SVG inline in image src attribute;<img src='data:image/svg+xml;utf8,<svg ... > ... </svg>'>

typed-php:
- Type name analog to PHP type keyword, bool, int, etc.
- Type mixed
- "Config::isStrict(), false: bool only accepts bool values, true: also boolLike values, yes, noo, true + false as string, 0, 1"
- Services, e.g. to Bool
- Readme, what is missing at forked project, No read mehtod which type value it is
- Type enum
- First version 0.1
Value objects which return other objects, clone the objects, else they can changed, if possible (public attiributes or setter methods)

###
# PHP

## x

* New name space concept.
* Object helper, hasParentInstance() getParentInstance() trait
* Value Object repo
* Data object field repo
* Twig builder
* Build Classes via Twig
* PHP part syntax check
* Convert PHP token data back to PHP code, Syntax check for parts, Syntax check for whole document
*  Symfony Events for stuff like Data object field on value change

## (TODOs)
Signal messenger installieren

## Custom HTML part parser

Base classes for Custom x part parser
Classes for HTML.

## Own typed repo
### type definition
Define the type. Also type combinations. Type from expression.

Perhaps type with properties?
Example ID, int and values >= 0?
Or better via Value object?
### type
ceck type definition against the current expressionn type.

