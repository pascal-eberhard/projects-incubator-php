# My repository list 

## Data

### Full

```yaml
_meta: ~

config-php:
  urls:
    - "https://bitbucket.org/pascal-eberhard/config-php/"
config-simple-php:
  packagist.org:
    active: true
    note: "github"
  urls:
    - "https://bitbucket.org/pascal-eberhard/config-simple-php/"
    - "https://github.com/pascal-eberhard/config-simple-php"
data-object-field-php:
  urls:
    - "https://bitbucket.org/pascal-eberhard/data-object-field-php/"
data-object-php:
  packagist.org:
    active: true
  urls:
    - "https://bitbucket.org/pascal-eberhard/data-object-php/"
datastore:
  urls:
    - "https://bitbucket.org/pascal-eberhard/datastore/"
docu-php:
  urls:
    - "https://bitbucket.org/pascal-eberhard/docu-php/"
io-php:
  urls:
    - "https://bitbucket.org/pascal-eberhard/io-php/"
library-template-php:
  urls:
    - "https://bitbucket.org/pascal-eberhard/library-template-php/"
projects-incubator-php:
  urls:
    - "https://bitbucket.org/pascal-eberhard/projects-incubator-php/"
project-tests-php:
  packagist.org:
    active: true
  urls:
    - "https://bitbucket.org/pascal-eberhard/project-tests-php/"
project-tools-php:
  packagist.org:
    active: true
  urls:
    - "https://bitbucket.org/pascal-eberhard/project-tools-php/"
testing-php:
  urls:
    - "https://bitbucket.org/pascal-eberhard/testing-php/"
twig-to-svg:
  urls:
    - "https://bitbucket.org/pascal-eberhard/twig-to-svg/"
typed-php:
  urls:
    - "https://bitbucket.org/pascal-eberhard/typed-php/"
utils-php:
  packagist.org:
    active: true
  urls:
    - "https://bitbucket.org/pascal-eberhard/utils-php/"
website-content-interface-php:
  urls:
    - "https://bitbucket.org/pascal-eberhard/website-content-interface-php/"

```

### Short

```yaml

_meta:
  fields:
    1:
      name: done
      note: "Is TODO done?"
    2:
      name: key
      note: "Repo key, see full list, above"
  format: YAML-CSV
done:
- [ false, "config-php" ]
- [ false, "config-simple-php" ]
- [ false, "data-object-field-php" ]
- [ false, "data-object-php" ]
- [ false, "datastore" ]
- [ false, "docu-php" ]
- [ false, "io-php" ]
- [ false, "library-template-php" ]
- [ false, "projects-incubator-php" ]
- [ false, "project-tests-php" ]
- [ false, "project-tools-php" ]
- [ false, "testing-php" ]
- [ false, "twig-to-svg" ]
- [ false, "typed-php" ]
- [ false, "utils-php" ]
- [ false, "website-content-interface-php" ]

```

## TODOs

### Readme and licence rework

* File names complete in upper case. It seems else packagist not rads the readme.

DONE:

```yaml

_meta:
  fields:
    1:
      name: done
      note: "Is TODO done?"
    2:
      name: key
      note: "Repo key, see full list, above"
  format: YAML-CSV
done:
- [ false, "config-php" ]
- [ false, "data-object-field-php" ]
- [ false, "data-object-php" ]
- [ false, "datastore" ]
- [ false, "docu-php" ]
- [ false, "io-php" ]
- [ false, "library-template-php" ]
- [ false, "projects-incubator-php" ]
- [ false, "project-tests-php" ]
- [ false, "project-tools-php" ]
- [ false, "testing-php" ]
- [ false, "twig-to-svg" ]
- [ false, "typed-php" ]
- [ false, "website-content-interface-php" ]

- [ true, "config-simple-php" ]
- [ true, "utils-php" ]

```

### Rework branches

* Rename `master` to `main` branch
* Delete all other branches, if not needed. E.g only docu or test project

DONE:

```yaml

_meta:
  fields:
    1:
      name: done
      note: "Is TODO done?"
    2:
      name: key
      note: "Repo key, see full list, above"
  format: YAML-CSV
done:
- [ false, "config-php" ]
- [ false, "data-object-field-php" ]
- [ false, "data-object-php" ]
- [ false, "datastore" ]
- [ false, "docu-php" ]
- [ false, "io-php" ]
- [ false, "library-template-php" ]
- [ false, "project-tools-php" ]
- [ false, "testing-php" ]
- [ false, "twig-to-svg" ]
- [ false, "typed-php" ]
- [ false, "website-content-interface-php" ]

- [ true, "config-simple-php" ]
- [ true, "projects-incubator-php" ]
- [ true, "project-tests-php" ]
- [ true, "utils-php" ]

```
