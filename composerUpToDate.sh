#!/usr/bin/env sh
# Shell: (sh composerUpToDate.sh)

# First simple syntax checks
addCount=0
binComposer=$(which composer)
deleteCount=0

rm -f var/tmp/composer-up-to-date.txt
$binComposer update --dry-run --no-ansi --no-scripts > var/tmp/composer-up-to-date.txt
exit;

find $(pwd) -iname "*.php" -type f -print | grep -Eiv "(var|vendor)/" | sort > var/tmp/temp-syntax-test-files.txt
for line in `cat var/tmp/temp-syntax-test-files.txt`
do
  count=$($binPHP -l $line | grep -iv "no syntax errors" | wc -l)
  errorCount=$(( $count + $errorCount ))
done

rm -f var/tmp/composer-up-to-date.txt
if [ $errorCount -gt 0 ]
then
  exit 1;
fi
exit 0;

composer update --dry-run --no-ansi --no-scripts | grep -Ei "\d+\s+(installs|removal)"
