# External content

This package contains package-external classes and files.

I find it easier to develop stuff for new libraries direct in the current repository. Later it can be moved to a own library or project.

To better differ between external and internal stuff, lets visualize it direct in the directory structure and the namespace.

So planned external stuff is always located in the "src/External" folder or rather in the `[insert package namespace, see composer.json]\External\*` namespace.

That means other developers can also use the external content. But should be aware that it can be moved to another repository. 
