#!/bin/env php
<?php declare(strict_types=1);

$baseDir = \realpath(__DIR__ . '/../..') . \DIRECTORY_SEPARATOR;

function xDump($data, bool $prettyPrint = true)
{
    print \PHP_EOL . '(JSON)' . \PHP_EOL;
    $options = (0
        + \JSON_OBJECT_AS_ARRAY
        + \JSON_BIGINT_AS_STRING
        + \JSON_UNESCAPED_SLASHES
        + \JSON_UNESCAPED_UNICODE
        + \JSON_PRESERVE_ZERO_FRACTION
    );
    if ($prettyPrint) {
        $options += \JSON_PRETTY_PRINT;
    }
    print \json_encode($data, $options);
    print \PHP_EOL . '(/JSON)' . \PHP_EOL;
}

//require_once __DIR__ . '/../../vendor/autoload.php';
require_once $baseDir . 'vendor/autoload.php';

use Symfony\Component\Finder;
use Symfony\Component\Yaml;

function xBuildPhar(string $filePath, bool $minify = false)
{
    if ('' == $filePath) {
        throw new \InvalidArgumentException('$filePath must not be empty');
    }

    $pharFile = new Finder\SplFileInfo($filePath, '', '');
    if ('phar' != $pharFile->getExtension()) {
        throw new \InvalidArgumentException('$filePath must contain a .phar file path '
            . \sprintf('"%s"', $pharFile->getPathname())
        );
    } elseif ('' == $pharFile->getFilenameWithoutExtension()) {
        throw new \InvalidArgumentException('$filePath not contains a file name, before the extension '
            . \sprintf('"%s"', $pharFile->getPathname())
        );
    }
    $baseDir = new Finder\SplFileInfo(\dirname($filePath), '', '');
    if (!$baseDir->isDir()) {
        throw new \InvalidArgumentException('$filePath directory not found '
            . \sprintf('"%s"', $baseDir->getPathname())
        );
    }

    $finder = (new Finder\Finder())
        ->files()
        ->in($baseDir->getRealPath())
        ->exclude('.idea', 'tests')
        ->ignoreVCS(true)
//        ->ignoreVCSIgnored(true)
        ->notName('.editorconfig')
        ->sortByName()
    ;
    if ($finder->count() < 1) {
        throw new \LogicException('No files found in '. \sprintf('"%s"', $baseDir->getPathname()));
    }

    if ($pharFile->isFile()) {
        \unlink($pharFile->getPathname());
    }

    $pharFileGz = new Finder\SplFileInfo($filePath . '.gz', '', '');
    if ($pharFileGz->isFile()) {
        \unlink($pharFileGz->getPathname());
    }

    $pharFileName = $pharFile->getFilename();
    $phar = new \Phar($pharFile->getPathname(), 0, $pharFileName);
    $phar->startBuffering();

    /** @var Finder\SplFileInfo $file */
    foreach ($finder as $file) {
        print \PHP_EOL . $file->getRelativePath();
        if ('' == $file->getPathname()) {
            continue;
//        } elseif ($pharFileName == $file->getRelativePath()) {
//            continue;
//        } elseif ($pharFileName . '.gz' == $file->getRelativePath()) {
//            continue;
        }

        $minifyData = '';
        if ($minify) {
            $minifyData = xMinify($file);
        }

        if ('' == $minifyData) {
            $phar->addFile($file->getPathname());
        } else {
            $phar->addFromString($file->getPathname(), $minifyData);
        }
    }

    if (\Phar::canCompress()) {
        $phar->compress(\Phar::GZ);
    }
    $phar->stopBuffering();
    #$phar->setStub();
    #$phar->setDefaultStub();
    #Phar::createDefaultStub

    //    $pharFile->getPath(),
//    $pharFile->getBasename(),
//    $pharFile->getFilename(),
//    $pharFile->getPathname(),
//    "0": "F:\\vagrant\\phar-test-php",
//    "1": "test.phar",
//    "2": "test.phar",
//    "3": "F:\\vagrant\\phar-test-php\\test.phar",



}

function xMinify(Finder\SplFileInfo $file)
{
    if (!$file->isFile()) {
        return '';
    } elseif ($file->getSize() < 1) {
        return '';
    }

    $extention = $file->getExtension();
    if ('' == $extention) {
        return '';
    }

    $extention = \mb_strtolower($extention, 'utf8');

    if ('json' == $extention) {
        return \json_encode(\json_decode($file->getContents(), true));
    } elseif ('php' == $extention) {
        return \php_strip_whitespace($file->getPathname());
    } elseif ('yaml' == $extention || 'yml' == $extention) {
        return Yaml\Yaml::dump(Yaml\Yaml::parse($file->getContents()), 0, 1);
    }

    if ('composer.lock' == $file->getFilename()) {
        return \json_encode(\json_decode($file->getContents(), true));
    }

    return '';
}

xBuildPhar($baseDir . 'test.phar', true);
//#$pharFile = new Finder\SplFileInfo($baseDir . 'test.phar', '', '');
///** @var Finder\SplFileInfo $file */
//$dump = [
//    'baseDir' => $baseDir,
//];
//
//
//xDump($dump);


exit;
