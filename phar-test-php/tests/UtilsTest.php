<?php declare(strict_types=1);

/*
 * This file is part of the REPLACE__projectKeyShort__END package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the license.md
 */

namespace PEPrograms\REPLACE__phpNamespaceKey__END\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\REPLACE__phpNamespaceKey__END\Utils
 * @copyright REPLACE__year__END Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/UtilsTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class UtilsTest extends TestCase
{
    /**
     * @covers ::doSomething
     * Shell: (vendor/bin/phpunit tests/UtilsTest.php --filter '/::testDoSomething\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testDoSomething()
    {
        $this->assertTrue(true);
    }
}
