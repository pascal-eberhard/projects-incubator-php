# Some replace and rework

## 1 Replace variables

Rework the replace values, matching to the current package.

```yaml

-
  replace: "config-simple-php" # E.g. The classes prefix text "This file is part of .."
  variable: "{{ package.name }}"
-
  replace: "ConfigSimple" # Package namespace part, e.g. "ConfigSimple"
  variable: "{{ package.nameSpace }}"
-
  replace: "2019" # Current year, e.g. for the copyright hint
  variable: "{{ year }}"
```

## 2 Rename files

```yaml

-
  from: "*.php.txt"
  to: "*.php"
```

## 3 Delete this

Delete this file.
