# Static Website Builder

Cache website by building compiling dynamic sources into static website files. Surely only useable for simple websites.

[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.0-8892BF.svg?style=flat-square)](https://php.net/)

## Properties

| Keyword | Value | Comment |
|----|----|----|
| Code Style Check | squizlabs | - |
| Programming language | PHP | - |
| Unit Tests | PHPUnit | - |

## Resources

* [Report issues](https://github.com/PascalEberhardProgramming/StaticWebsiteBuilder/issues)

## Usage

### Install via composer

````json
{
    // ..
    "repositories": [
        {
            "type": "vcs",
            "url": "https://github.com/PascalEberhardProgramming/StaticWebsiteBuilder.git"
        }
    ],
    "require": {
        "PascalEberhardProgramming/StaticWebsiteBuilder": ">=1.0.0"
    }
    // ..
}
````

### PHP

````php
// ...
````
