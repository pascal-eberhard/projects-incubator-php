<?php

/*
 * This file is part of the Unit Tests by Annotations package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\UnitTestByAnnotation;

/**
 * Some utils
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Utils
{
    
    /**
     * Add directory seperator if not set
     * 
     * @param string $path
     * @return string
     */
    public static function dirPathAddSeperator(string $path): string
    {
        if (DIRECTORY_SEPARATOR != mb_substr($path, -1, 1, Config::CHARSET)) {
            $path .= DIRECTORY_SEPARATOR;
        }
        return $path;
    }
}
