# CHANGELOG

## 1.0.0

* First stable version
* PHPUnit tested
* Ready to be used via composer, with repositories entry
* Currently the logger only logs to file and to JSON format

This is the first stable uncomplex version. Optimizing at code and logic are tasks for later versions.

