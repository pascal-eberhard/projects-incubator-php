<?php

/*
 * This file is part of the Block-wise Logger package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\BlockwiseLogger\Tests\Unit\Logger;

use PascalEberhardProgramming\BlockwiseLogger\Logger\JsonLogger;
use PascalEberhardProgramming\BlockwiseLogger\Utils;
use PHPUnit\Framework\TestCase;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PascalEberhardProgramming\BlockwiseLogger\Logger\JsonLogger
 */
class JsonLoggerTest extends TestCase
{
    
    /**
     * Expected log file content
     * 
     * @var string
     */
    const LOG = '[' . PHP_EOL . '{"a":1,"0":true,"1":null},' . PHP_EOL
        . '1492356258.585898,' . PHP_EOL . '"Käße"' . PHP_EOL . ']'
    ;
    
    /**
     * Test log file dirPath
     * 
     * @var string
     */
    protected $filePath = '';

    /**
     * Data provider for ::testConstructExceptions()
     * 
     * @return array
     */
    public function dataConstructExceptions(): array
    {
        return [
            // integer $input // invalid input
            [''],
            // Funny, at Windows, the below file is created as
            // .. ".\\this-dirPath-should-not-exist-565446445646634456642456645f1zhsdg61h6fg4u3566u8"
//            ['this-dirPath-should-not-exist-565446445646634456642456645f1zhsdg61h6fg4u3566u8'],
        ];
    }

    /**
     * Create test log file path
     *
     * @return string
     */
    public function getFilePath(): string
    {
        if ('' == $this->filePath) {
            if (isset($_SERVER['TMP'])) {
                $this->filePath = Utils::dirPathAddSeperator($_SERVER['TMP'])
                    . 'test-log-43654234452344526543456543252.json'
                ;
            } else {
                $this->filePath = Utils::dirPathAddSeperator(__DIR__)
                    . 'test-log-43654234452344526543456543252.json'
                ;
            }
        }
        return $this->filePath;
    }

    /**
     * Stuff before each test
     */
    public function setUp()
    {
        // Delete old test file
        if (is_file($this->getFilePath())) {
            unlink($this->filePath);
        }
    }

    /**
     * @covers ::__construct
     * @dataProvider dataConstructExceptions
     * @expectedException \InvalidArgumentException
     * 
     * @param string $input
     */
    public function testConstructExceptions(string $input)
    {
        $log = new JsonLogger($input);
    }

    /**
     * @covers ::__construct
     * @covers ::log
     */
    public function testDefault()
    {
        $log = new JsonLogger($this->getFilePath());
   
        $log->log(['a' => 1, true, null]);
        $log->log(1492356258.585898);
        $log->log('Käße');
        
        // Close file handle
        unset($log);
        
        // Check content
        $data = file_get_contents($this->getFilePath());
        $this->assertEquals(self::LOG, $data);
    }

    /**
     * @covers ::getPath
     */
    public function testGetPath()
    {
        $log = new JsonLogger($this->getFilePath());
        $this->assertEquals($this->getFilePath(), $log->getPath());
    }

    /**
     * @expectedException \LogicException
     */
    public function testNoCloning()
    {
        $log = new JsonLogger($this->getFilePath());
        clone($log);
    }
}
