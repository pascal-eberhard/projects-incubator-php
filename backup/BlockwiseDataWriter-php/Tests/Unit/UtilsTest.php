<?php

/*
 * This file is part of the Block-wise Logger package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\BlockwiseLogger\Tests\Unit;

use PascalEberhardProgramming\BlockwiseLogger\Utils;
use PHPUnit\Framework\TestCase;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PascalEberhardProgramming\BlockwiseLogger\Utils
 */
class UtilsTest extends TestCase
{

    /**
     * Data provider for ::testDirPathAddSeperator()
     * 
     * @return array
     */
    public function dataDirPathAddSeperator(): array
    {
        return [
            // string $expectedOutput, string $input
            [DIRECTORY_SEPARATOR, ''],
            [DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR],
            [DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR],
            ['x' . DIRECTORY_SEPARATOR . 'x' . DIRECTORY_SEPARATOR, 'x' . DIRECTORY_SEPARATOR . 'x'],
            ['x' . DIRECTORY_SEPARATOR . 'x' . DIRECTORY_SEPARATOR, 'x' . DIRECTORY_SEPARATOR . 'x' . DIRECTORY_SEPARATOR],
            ['Käße' . DIRECTORY_SEPARATOR, 'Käße'],
            ['Käße' . DIRECTORY_SEPARATOR, 'Käße' . DIRECTORY_SEPARATOR],
        ];
    }

    /**
     * Data provider for ::testStringGetByteCount()
     * 
     * @return array
     */
    public function dataStringGetByteCount(): array
    {
        return [
            // integer $expectedCount, string $input
            [0, ''],
            [1, 'x'],
            
            // Cheese in german, with and without special chars
            // .. Only for testing, the correct word is "Käse" :)
            [5, 'Kaese'],
            [6, 'Käße'],
        ];
    }

    /**
     * @covers ::dirPathAddSeperator
     * @dataProvider dataDirPathAddSeperator
     * 
     * @param string $expectedOutput
     * @param string $input
     */
    public function testDirPathAddSeperator(string $expectedOutput, string $input)
    {
        $this->assertEquals($expectedOutput, Utils::dirPathAddSeperator($input));
    }

    /**
     * @covers ::stringGetByteCount
     * @dataProvider dataStringGetByteCount
     * 
     * @param integer $expectedCount
     * @param string $input
     */
    public function testStringGetByteCount(int $expectedCount, string $input)
    {
        $this->assertEquals($expectedCount, Utils::stringGetByteCount($input));
    }
}
