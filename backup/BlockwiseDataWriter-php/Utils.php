<?php

/*
 * This file is part of the Block-wise Logger package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\BlockwiseLogger;

/**
 * Some utils
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Utils
{
    
    /**
     * Add directory seperator if not set
     * 
     * @param string $path
     * @return string
     */
    public static function dirPathAddSeperator(string $path): string
    {
        if (DIRECTORY_SEPARATOR != mb_substr($path, -1, 1, Config::CHARSET)) {
            $path .= DIRECTORY_SEPARATOR;
        }
        return $path;
    }
    
    /**
     * Check JSON error
     * 
     * @param string $label
     * @throws \LogicException At JSON error(s)
     */
    protected static function jsonCheckError(string $label)
    {
        $error = \json_last_error();
        if (0 == $error) {
            return;
        }
        throw new \LogicException('Error at ' . $label . ', errorCode: ' . $error . ', errorText: ' . \json_last_error_msg());
    }
    
    /**
     * JSON encode, mixed data to string
     * Hanlde errors
     * 
     * @param mixed $input
     * @param integer $options Optional, See constants JSON_*, default: 0
     * @param integer $depth Optional
     * @return string
     */
    public static function jsonEncode($input, int $options = 0, int $depth = 0): string
    {
        if (0 == $depth) {
            $output = \json_encode($input, $options);
        } else {
            $output = \json_encode($input, $options, $depth);
        }
        Utils::jsonCheckError('json_encode()');
        return $output;
    }
    
    /**
     * Get byte count of string
     * 
     * @param string $data
     * @return integer
     */
    public static function stringGetByteCount(string $data): int
    {
        // As own method, to add corrections intern, if needed
        return strlen($data);
    }
}
