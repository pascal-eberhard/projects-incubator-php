# Simple block-wise logger

A simple logger for small projects. Log block-wise. A block is a valid sub-element of a specific data format, e.g. a JSON value.

[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.0-8892BF.svg?style=flat-square)](https://php.net/)

(Currently the logger only logs to file and to JSON format).

## Properties

| Keyword | Value | Comment |
|----|----|----|
| Code Style Check | squizlabs | - |
| Programming language | PHP | - |
| Unit Tests | PHPUnit | - |

## Resources

* [Report issues](https://github.com/PascalEberhardProgramming/php_blockwise_logger/issues)

## Usage

### Install via composer

````json
{
    // ..
    "repositories": [
        {
            "type": "vcs",
            "url": "https://github.com/PascalEberhardProgramming/php_blockwise_logger.git"
        }
    ],
    "require": {
        "PascalEberhardProgramming/php_blockwise_logger": ">=1.0.0"
    }
    // ..
}
````

### PHP

````php
// ...

// (If needed)
require_once __DIR__ . '/vendor/autoload.php';

// ...

use PascalEberhardProgramming\BlockwiseLogger\Logger\JsonLogger;

// ...

$filePath = '[Replace with path prefix]/log.json';

$log = new JsonLogger($filePath);

$log->log(true);
// ...
$log->log('this is a text');

````
The above PHP code will generate the following JSON
````json
[
true,
"this is a text"
]

````
After every ::log() call, the log file is a valid JSON file.