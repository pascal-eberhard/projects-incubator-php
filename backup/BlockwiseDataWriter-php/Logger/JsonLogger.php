<?php

/*
 * This file is part of the Block-wise Logger package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\BlockwiseLogger\Logger;

use PascalEberhardProgramming\BlockwiseLogger\Utils;

/**
 * JSON file logger
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class JsonLogger
{
    
    /**
     * Content prefix
     * Not const, because const is always public
     * 
     * @var string
     */
    protected static $contentPrefix = '[' . PHP_EOL;
    
    /**
     * Content suffix
     * Not const, because const is always public
     * 
     * @var string
     */
    protected static $contentSuffix = PHP_EOL . ']';
    
    /**
     * Content suffix length
     * 
     * @var integer
     */
    protected static $contentSuffixDiff = 0;
    
    /**
     * File empty
     * 
     * @var boolean
     */
    protected $fileEmpty = false;
    
    /**
     * File exist?
     * 
     * @var boolean
     */
    protected $fileExist = false;
    
    /**
     * File handle
     * 
     * @var resource
     */
    protected $fileHanlde = null;
    
    /**
     * File path
     * 
     * @var string
     */
    protected $path = '';

    /**
     * Constructor
     *
     * @param string $path Log filePath, dirPath allowed
     * If $path is directory, filename is generated randomly
     *
     * @throws \InvalidArgumentException At parameter error(s)
     */
    public function __construct(string $path)
    {
        // Init and paramter
        if ('' == $path) {
            throw new \InvalidArgumentException('$path must not be empty');
        }
        
        // Build log file data
        if (is_dir($path)) {
            $this->path = Utils::dirPathAddSeperator($path)
                . date('Ymd-His-' . str_replace('.', '-', '' . microtime(true)))
                . '.json'
            ;
        } else if (is_file($path)) {
            $this->path = $path;
        } else {
            $dirPath = dirname($path);
            $fileName = basename($path);
            if ('' != $dirPath && is_dir($dirPath) && '' != $fileName) {
                $this->path = Utils::dirPathAddSeperator($dirPath) . $fileName;
            } else {
                throw new \InvalidArgumentException('$path must be a valid '
                    . 'directory - or file path, ' . var_export($path, true)
                );
            }
        }
        $this->fileEmpty = true;
        $this->fileExist = is_file($this->path);
        if ($this->fileExist) {
            $this->fileEmpty = (filesize($path) < 1);
        }
        
        if (0 == self::$contentSuffixDiff) {
            $diff = Utils::stringGetByteCount(self::$contentSuffix);
            self::$contentSuffixDiff = -1 * $diff;
        }
    }

    /**
     * Destructor
     *
     * @throws \LogicException At error
     */
    public function __destruct()
    {
        if (null !== $this->fileHanlde) {
            if (false === fclose($this->fileHanlde)) {
                throw new \LogicException('Error at closing file handle');
            }
        }
    }

    /**
     * Cloning not allowed
     *
     * @throws \LogicException Cloning not allowed
     */
    public function __clone()
    {
        throw new \LogicException(__CLASS__ . ' uses open file handles.');
    }

    /**
     * Get log file hanlde
     *
     * @return resource
     * @throws \LogicException At error
     */
    protected function getFileHandle()
    {
        if (null === $this->fileHanlde) {
            if (!$this->fileExist) {
                file_put_contents($this->path, '');
                $this->fileExist = true;
            }
            // "w+" not "a+", else fseek() at ::logIntern() has no effect
            $this->fileHanlde = fopen($this->path, 'w+');
            if (false === $this->fileHanlde) {
                throw new \LogicException('Error at fopen()');
            }
            $res = fseek($this->fileHanlde, 0, SEEK_END);
            if (0 != $res) {
                throw new \LogicException('Error at fseek()');
            }
        }
        return $this->fileHanlde;
    }

    /**
     * Get log file path
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Log block
     *
     * @param string $data
     * @throws \LogicException At error
     */
    protected function logIntern(string $data)
    {
        $fileExists = $this->fileExist;
        $this->getFileHandle();
        $prefix = '';
        if (!$fileExists || $this->fileEmpty) {
            $prefix = self::$contentPrefix;
        }
        if ($this->fileEmpty) {
            fputs($this->fileHanlde, $prefix . $data . self::$contentSuffix);
        } else {
            $res = fseek($this->fileHanlde, self::$contentSuffixDiff, SEEK_END);
            if (0 != $res) {
                throw new \LogicException('Error at fseek()');
            }
            $content = ',' . PHP_EOL . $data . self::$contentSuffix;
            fputs($this->fileHanlde, $content);
        }
        $this->fileEmpty = false;
    }

    /**
     * Log block
     *
     * @param mixed $data
     */
    public function log($data)
    {
        $this->logIntern(Utils::jsonEncode($data
            , JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE
        ));
    }
}
