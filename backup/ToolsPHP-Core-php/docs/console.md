# Console commands

## Code sniffer

### Windows

````bash
sh bin/phpcs -s > ../zz_cs.txt
````

## GIT

### Store Credentials

````bash
git config credential.helper store
````

### Tagging

````bash
# Replace all finds for the project version
# Commit and push, then the following
git tag <x>.<y>.<z>
git push origin --tags
````

## PHPUnit

### Windows

````bash
sh bin/phpunit
````
