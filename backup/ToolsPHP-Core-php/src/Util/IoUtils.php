<?php

/*
 * This file is part of the Tools PHP Base package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\ToolsPHP\Base\Util;

use PascalEberhardProgramming\ToolsPHP\Config;

/**
 * I/O, file and directory, utils
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class IoUtils
{
    
    /**
     * Add directory seperator if not set
     * 
     * @param string $path
     * @return string
     */
    public static function dirPathAddSeperator(string $path): string
    {
        Config::get('x');
        if (DIRECTORY_SEPARATOR != mb_substr($path, -1, 1, Config::CHARSET)) {
            $path .= DIRECTORY_SEPARATOR;
        }
        return $path;
    }
}
