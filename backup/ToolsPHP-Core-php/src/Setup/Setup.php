<?php

/*
 * This file is part of the Tools PHP Base package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\ToolsPHP\Base\Setup;

use Symfony\Component\Yaml\Json;

/**
 * Package setup at composer install / update
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Setup
{

    /**
     * Default charset
     *
     * @var string
     */
    const CHARSET = 'utf8';

    /**
     * Base dir
     *
     * @var string
     */
    private $basePath = '';

    /**
     * Composer name
     *
     * @var null|string[]
     */
    private $composerNameParts = null;

    /**
     * Config data
     *
     * @var array|null
     */
    private $config = null;

    /**
     * Constructor
     *
     * @param string $basePath
     * @throws \InvalidArgumentException
     */
    public function __construct(string $basePath)
    {
        // Parameter
        if ('' == $basePath) {
            throw new \InvalidArgumentException('$basePath must not be empty');
        }
        if (!is_dir($basePath)) {
            throw new \InvalidArgumentException('$basePath is no valid dir');
        }

        // Set data
        if (DIRECTORY_SEPARATOR != mb_substr($basePath, -1, 1, self::CHARSET)) {
            $basePath .= DIRECTORY_SEPARATOR;
        }
        $this->basePath = $basePath;
    }

    /**
     * Load composer.json
     *
     * @param string $pathSuffix File path part after basePath
     * @throws \InvalidArgumentException
     */
    public function loadComposerJson(string $pathSuffix)
    {
        // Parameter
        if ('' == $pathSuffix) {
            throw new \InvalidArgumentException('$pathSuffix must not be empty');
        }

        // Actions
        $file = $this->basePath . $pathSuffix;
        if (!is_file($file)) {
            throw new \InvalidArgumentException('$file is no valid file');
        }

        $data = \json_decode(file_get_contents($file), true, 512, JSON_BIGINT_AS_STRING);
        $errorCode = \json_last_error();
        if (JSON_ERROR_NONE != $errorCode) {
            $error = 'Error at json_decode(), error(#' . $errorCode
                . '; "' . \json_last_error_msg() . '"'. ')';
            throw new \RuntimeException($error);
        }

        if (!isset($data['name'])) {
            throw new \LogicException('No name entry set in composer.json');
        }
        $data = $data['name'];
        if (!(is_string($data) && '' != $data)) {
            throw new \LogicException('Name entry in composer.json must be string and not empty');
        }
        $this->composerNameParts = explode('/', $data);
    }

    /**
     * Load config.X
     *
     * @param string $pathSuffix File path part after basePath
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function loadConfig(string $pathSuffix)
    {
        // Requirements
        if (null === $this->composerNameParts) {
            throw new \LogicException('Composer data not loaded, use ::loadComposerJson()');
        }

        // Parameter
        if ('' == $pathSuffix) {
            throw new \InvalidArgumentException('$pathSuffix must not be empty');
        }

        // Actions
        $file = $this->basePath . $pathSuffix;
        if (!is_file($file)) {
            throw new \InvalidArgumentException('$file is no valid file');
        }

        $ext = pathinfo($pathSuffix, PATHINFO_EXTENSION );
        if (!(is_string($ext) && '' != $ext)) {
            throw new \InvalidArgumentException('$pathSuffix, no valid file extension found');
        }
        $ext = mb_strtolower($ext, self::CHARSET);
        $data = file_get_contents($file);
//        $data = $this->replaceVariables(file_get_contents($file));
        $this->config = $data;
//        switch ($ext) {
//            case 'yml':
//                $data = Yaml::parse($data);
//                break;
//
//            default:
//                throw new \LogicException('$pathSuffix, file type "' . $ext . '" not supported yet');
//        }
//
//        if (!(is_array($data) && !empty($data))) {
//            throw new \LogicException('Unable to parse data');
//        }
//
//        // - Get package specific data
//        foreach ($this->composerNameParts as $key) {
//            if (!isset($data[$key])) {
//                $error = 'Invalid config file format, data must be prefixed with composer.json name parts'
//                    . ' (separated by "/")';
//                throw new \LogicException($error);
//            }
//            $data = $data[$key];
//        }
//        $this->config = $data;
    }

    /**
     * Replace variables
     *
     * @param string $data
     * @return string
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function replaceVariables(string $data): string
    {
        if ('' == $data) {
            return '';
        }
        if (false === mb_strpos($data, '%', 0, self::CHARSET)) {
            return $data;
        }

        // Actions
        $vars = [
            'basePath' => $this->basePath,
        ];
        foreach ($vars as $var => $value) {
            // There is no mb_str_replace, so preg_replace with /u
            $regEx = preg_quote('%' . $var . '%', '/');
            $data = preg_replace('/' . $regEx . '/iu', $data, $value);
        }

        return $data;
    }
}
