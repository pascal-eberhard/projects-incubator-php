<?php
/**
 * This file is part of the Tools PHP JSON package.
 *
 * PHP version 7
 *
 * @package    ToolsPHP
 * @subpackage JSON
 * @author     Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright  2017-2017 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @license    <https://github.com/PascalEberhardProgramming/ToolsPHP-Json/blob/master/LICENSE> MIT License
 */

namespace PascalEberhardProgramming\ToolsPHP\Json;

use PascalEberhardProgramming\ToolsPHP\Json\Exception\JsonException;

/**
 * Wrapper for PHPs build in JSON logic, with automatically error check
 */
class Json
{

    /**
     * Check for JSON error
     * Checked automatically at the classes convert methods
     *
     * @throws \PascalEberhardProgramming\ToolsPHP\Json\Exception\JsonException At error
     */
    public static function checkError()
    {
        $code = \json_last_error();

        if (JSON_ERROR_NONE != $code) {
            throw new JsonException(\json_last_error_msg(), $code);
        }
    }

    /**
     * Convert JSON string to PHP value
     *
     * @see PHP json_decode <http://php.net/manual/en/function.json-decode.php>
     * @param string $json
     * @param bool $assoc Optional, default: FALSE
     * @param int $depth Optional, default: 512
     * @param int $options Optional, default: 0
     * @return mixed The JSON converted to a PHP value
     * @throws \PascalEberhardProgramming\ToolsPHP\Json\Exception\JsonException At error
     */
    public static function decode(string $json, bool $assoc = false, int $depth = 512, int $options = 0)
    {
        $res = \json_decode($json, $assoc, $depth, $options);

        // There is no thread-atomic logic in PHP, so hard-copy self::checkError()
        $code = \json_last_error();

        if (JSON_ERROR_NONE != $code) {
            throw new JsonException(\json_last_error_msg(), $code);
        }

        return $res;
    }

    /**
     * Convert PHP value to JSON string
     *
     * @see PHP json_encode <http://php.net/manual/en/function.json-encode.php>
     * @param mixed $value
     * @param int $options Optional, default: 0
     * @param int $depth Optional, default: 512
     * @return string The PHP value converted to JSON
     * @throws \PascalEberhardProgramming\ToolsPHP\Json\Exception\JsonException At error
     */
    public static function encode($value, int $options = 0, int $depth = 512): string
    {
        $res = \json_encode($value, $options, $depth);

        // There is no thread-atomic logic in PHP, so hard-copy self::checkError()
        $code = \json_last_error();

        if (JSON_ERROR_NONE != $code) {
            throw new JsonException(\json_last_error_msg(), $code);
        }

        return $res;
    }
}
