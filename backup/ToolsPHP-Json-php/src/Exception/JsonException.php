<?php
/**
 * This file is part of the Tools PHP JSON package.
 *
 * PHP version 7
 *
 * @package    ToolsPHP
 * @subpackage JSON
 * @author     Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright  2017-2017 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @license    <https://github.com/PascalEberhardProgramming/ToolsPHP-Json/blob/master/LICENSE> MIT License
 */

namespace PascalEberhardProgramming\ToolsPHP\Json\Exception;

/**
 * Exception at JSON error
 */
class JsonException extends \LogicException
{
}
