<?php
/**
 * This file is part of the Tools PHP Base package.
 *
 * PHP version 7
 *
 * @package    ToolsPHP
 * @subpackage Base
 * @author     Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright  2017-2017 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @license    https://github.com/PascalEberhardProgramming/ToolsPHP-Base/blob/master/LICENSE MIT License
 */

namespace PascalEberhardProgramming\ToolsPHP\Base\Config;

/**
 * Config data
 * Class gets override by setup script
 */
class Config extends ConfigBase
{
}
