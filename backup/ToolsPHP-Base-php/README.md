# Tools PHP / Base package

Some lightweight framework for my projects. Base stuff for the other ToolsPHP repos.

[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.0-8892BF.svg?style=flat-square)](https://php.net/)

## Properties

| Keyword | Value | Comment |
|----|----|----|
| Code Style Check | squizlabs | - |
| Programming language | PHP | - |
| Unit Tests | PHPUnit | - |

## Resources

* [Report issues](https://github.com/PascalEberhardProgramming/ToolsPHP-Base/issues)

## Usage

### Install via composer

````json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://github.com/PascalEberhardProgramming/ToolsPHP-Base.git"
        }
    ],
    "require": {
        "PascalEberhardProgramming/ToolsPHP-Base": ">=0.0.3"
    }
}
````
