# Category Manager

Tool to handle Category trees. With patterns for ORM Entities and Web forms.

[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.0-8892BF.svg?style=flat-square)](https://php.net/)

## Properties

| Keyword | Value | Comment |
|----|----|----|
| Code Style Check | squizlabs | - |
| Programming language | PHP | - |
| Unit Tests | PHPUnit | - |

## Resources

* [Report issues](https://github.com/PascalEberhardProgramming/CategoryManager/issues)

## Usage

### Install via composer

(Version Tag not yet set).
````json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://github.com/PascalEberhardProgramming/CategoryManager.git"
        }
    ],
    "require": {
        "PascalEberhardProgramming/CategoryManager": ">=0.0.1"
    }
}
````

### PHP

````php
// ...
````
