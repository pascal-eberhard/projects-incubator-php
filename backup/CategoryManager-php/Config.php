<?php

/*
 * This file is part of the Category Manager package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\CategoryManager;

/**
 * Config data
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Config
{
    
    /**
     * Default charset
     * 
     * @var string
     */
    const CHARSET = 'utf8';
}
