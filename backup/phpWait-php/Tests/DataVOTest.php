<?php

/*
 * This file is part of the Wait package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhard\Wait\Tests;

use PascalEberhard\Wait\DataVO;
use PHPUnit\Framework\TestCase;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PascalEberhard\Wait\DataVO
 * Shell: (bin/phpunit Tests/DataVOTest.php)
 */
class VOTest extends TestCase
{

    /**
     * @return array
     * @see self::testDefault
     */
    public function dataDefault(): array
    {
        return [
            // int $total Expected total time,
            // int $time The wait time per step,
            // string|object $unit Optional, unit of $time, default:#,
            // int $steps Optional, how often to wait? Default:1

            [1, 1, '', 1],
            [10, 1, '', 10],
            [10, 5, '', 2],
        ];
    }
//
//    /**
//     * @return array
//     * @see self::testExceptions
//     */
//    public function dataExceptions(): array
//    {
//        return [
//            // string $class,
//            // string $messageRegex,
//            // string $data Input data,
//            // String\ConfigInterface|null $config
//
//            [Exception\EmptyDataException::class, '', '', (new StringData\ConfigDO())
//                ->setEmptyAllowed(false)
//            ],
//       ];
//    }

    /**
     * @covers ::__construct
     * @dataProvider dataDefault
     *
     * @param int $total Expected total time
     * @param int $time The wait time per step
     * @param string|object $unit Optional, unit of $time, default:#
     * @param int $steps Optional, how often to wait? Default:1
     * Shell: (bin/phpunit Tests/DataVOTest.php --filter testDefault)
     */
    public function testDefault(int $total, int $time, $unit, int $steps)
    {
        $vo = new DataVO($time, $unit, $steps);

        $this->assertEquals($steps, $vo->getSteps(), 'Steps');
        $this->assertEquals($time, $vo->getTime(), 'Time');
        $this->assertEquals($total, $vo->getTotalTime(), 'TotalTime');
    }
//
//    /**
//     * @covers ::__construct
//     * @dataProvider dataExceptions
//     *
//     * @param string $class
//     * @param string $messageRegex
//     * @param string $data Input data
//     * @param StringData\ConfigInterface|null $config
//     * Shell: (bin/phpunit Tests/Incubator/Core/ValueObject/StringData/VOTest.php --filter testExceptions)
//     */
//    public function testExceptions(string $class,
//                                   string $messageRegex,
//                                   string $data,
//                                   ?StringData\ConfigInterface $config
//    ) {
//        $this->expectException($class);
//        if ('' !== $messageRegex) {
//            $this->expectExceptionMessageRegExp($messageRegex);
//        }
//        new StringData\VO($data, $config);
//    }
}
