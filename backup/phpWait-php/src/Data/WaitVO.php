<?php
declare(strict_types=1);

/*
 * This file is part of the Wait package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhard\Wait;

/**
 * Value object for the wait data
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard
 */
class WaitVO
{

    /**
     * @var int
     */
    private $steps = 0;

    /**
     * @var int
     */
    private $time = 0;

    /**
     * @var int
     */
    private $total = 0;

    /**
     * @var string|object
     */
    private $unit = '';

    /**
     * Constructor
     *
     * @param int $time The wait time per step
     * @param string|object $unit Optional, unit of $time, default:#
     * @param int $steps Optional, how often to wait? Default:1
     * @param \InvalidArgumentException
     */
    public function __construct(int $time, $unit = '', $steps = 1)
    {
        // Init stuff
        if ($time < 1) {
            throw new \InvalidArgumentException('$time must be >= 1');
//        } elseif ('' === $unit) {
//            throw new \InvalidArgumentException('$unit must not be empty');
        } elseif ($steps < 1) {
            throw new \InvalidArgumentException('$steps must not be empty');
        }

        $this->steps = $steps;
        $this->time = $time;
        $this->unit = $unit;

        $this->total = $steps * $time;
    }

    /**
     * Get wait steps
     *
     * @return int
     */
    public function getSteps(): int
    {
        return $this->steps;
    }

    /**
     * Get wait time per step in seconds
     *
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * Get total wait time in seconds
     *
     * @return int
     */
    public function getTotalTime(): int
    {
        return $this->total;
    }
}
