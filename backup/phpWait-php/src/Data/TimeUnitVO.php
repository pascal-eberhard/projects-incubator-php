<?php
declare(strict_types=1);

/*
 * This file is part of the Wait package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhard\Wait\Data;

use MyCLabs\Enum;

/**
 * Value object for the time unit
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard
 */
class TimeUnitVO extends Enum\Enum
{

    /**
     * Days, in seconds
     *
     * @var int
     */
    const DAY = 86400;

    /**
     * Hours, in seconds
     *
     * @var int
     */
    const HOUR = 3600;

    /**
     * Micro seconds
     *
     * @var int
     */
    const MICRO_SECOND = 10000000;

    /**
     * Milli seconds
     *
     * @var int
     */
    const MILLI_SECOND = 1000;

    /**
     * Minutes, in seconds
     *
     * @var int
     */
    const MINUTE = 60;

    /**
     * Average months, in seconds
     *
     * @var int
     */
    const MONTH = 2629800;

    /**
     * Nano seconds
     *
     * @var int
     */
    const NANO_SECOND = 10000000000;

    /**
     * Seconds
     *
     * @var int
     */
    const SECOND = 60;

    /**
     * Weeks, in seconds
     *
     * @var int
     */
    const WEEK = 604800;

    /**
     * Average tropical years, in seconds
     *
     * @var int
     */
    const YEAR = 31557600;
}
