<?php
declare(strict_types=1);

/*
 * This file is part of the Wait package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhard\Wait;

/**
 * Handler to wait
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2019 Pascal Eberhard
 */
class Wait
{
    // @todo
}
