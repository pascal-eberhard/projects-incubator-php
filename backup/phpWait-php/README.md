# Wait a minute ..

Callback and value object to wait in PHP. 

X TODO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

## Properties

| Keyword | Value | Comment |
|----|----|----|
| Code Style Check | squizlabs | - |
| Programming language | PHP | - |
| Unit Tests | PHPUnit | - |

## Resources

* [Report issues](https://github.com/pascal-eberhard/phpWait/issues)

## Usage

### Install via composer

````json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://github.com/pascal-eberhard/phpWait.git"
        }
    ],
    "require": {
        "PascalEberhardProgramming/DataStore": ">=1.0.0"
    }
}
````

### PHP

````php
// ...
````
