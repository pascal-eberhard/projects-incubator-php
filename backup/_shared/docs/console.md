# Console commands

## Code sniffer

### Windows

````bash
sh bin/phpcs
````

## PHPUnit

### Windows

````bash
sh bin/phpunit
````
