# Unit Tests by Annotations

Specify all data for the unit tests via Annotations and generate it from this data.

[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.0-8892BF.svg?style=flat-square)](https://php.net/)

## Properties

| Keyword | Value | Comment |
|----|----|----|
| Code Style Check | squizlabs | - |
| Programming language | PHP | - |
| Unit Tests | PHPUnit | - |

## Resources

* [Report issues](https://github.com/PascalEberhardProgramming/UnitTestByAnnotation/issues)

## Usage

### Install via composer

````json
{
    // ..
    "repositories": [
        {
            "type": "vcs",
            "url": "https://github.com/PascalEberhardProgramming/UnitTestByAnnotation.git"
        }
    ],
    "require": {
        "PascalEberhardProgramming/UnitTestByAnnotation": ">=1.0.0"
    }
    // ..
}
````

### PHP

````php
// ...
````
