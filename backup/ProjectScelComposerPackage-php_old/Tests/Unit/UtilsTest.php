<?php

/*
 * This file is part of the projectNameLabel package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\projectNameKey\Tests\Unit;

use PascalEberhardProgramming\projectNameKey\Utils;
use PHPUnit\Framework\TestCase;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PascalEberhardProgramming\projectNameKey\Utils
 */
class UtilsTest extends TestCase
{

    /**
     * Data provider for ::testDirPathAddSeperator()
     * 
     * @return array
     */
    public function dataDirPathAddSeperator(): array
    {
        return [
            // string $expectedOutput, string $input
            [DIRECTORY_SEPARATOR, ''],
            [DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR],
            [DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR],
            ['x' . DIRECTORY_SEPARATOR . 'x' . DIRECTORY_SEPARATOR, 'x' . DIRECTORY_SEPARATOR . 'x'],
            ['x' . DIRECTORY_SEPARATOR . 'x' . DIRECTORY_SEPARATOR, 'x' . DIRECTORY_SEPARATOR . 'x' . DIRECTORY_SEPARATOR],
            ['Käße' . DIRECTORY_SEPARATOR, 'Käße'],
            ['Käße' . DIRECTORY_SEPARATOR, 'Käße' . DIRECTORY_SEPARATOR],
        ];
    }

    /**
     * @covers Utils::dirPathAddSeperator
     * @dataProvider dataDirPathAddSeperator
     * 
     * @param string $expectedOutput
     * @param string $input
     */
    public function testDirPathAddSeperator(string $expectedOutput, string $input)
    {
        $this->assertEquals($expectedOutput, Utils::dirPathAddSeperator($input));
    }
}
